﻿using System;
using System.Collections.Generic;
using XamarinApp.Models;

namespace XamarinApp
{
    public class ScreenshotList
    {
        public List<Screenshot> list;
        public event Action<Screenshot> ScreenshotAdded;

        public ScreenshotList()
        {
            list = new List<Screenshot>();
        }

        public void Add(Screenshot screenshot)
        {
            list.Add(screenshot);
            ScreenshotAdded?.Invoke(screenshot);
        }
    }
}