﻿using System;
using MediaManager;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific.AppCompat;
using Xamarin.Forms.Xaml;
using XamarinApp.Views;
using Application = Xamarin.Forms.Application;

namespace XamarinApp
{
    public partial class App : Application
    {
        public App()
        {
            On<Android>()
                .SendDisappearingEventOnPause(true)
                .SendAppearingEventOnResume(true)
                .ShouldPreserveKeyboardOnResume(true);

            InitializeComponent();

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
