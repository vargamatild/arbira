﻿namespace XamarinApp
{
    public interface IUnityMessageListener
    {
        void MessageFromUnity(string msg);
    }
}