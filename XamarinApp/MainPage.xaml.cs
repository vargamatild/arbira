﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using XamarinApp.Views;

namespace XamarinApp
{
    public partial class MainPage : ContentPage
    {
        public ICommand TapCommand => new Command<string>(async (url) => await Launcher.OpenAsync(url));
        private readonly HttpClient _client;

        public MainPage()
        {
            InitializeComponent();
            _client = new HttpClient();
            simulateButton.Clicked += SimulateButtonClicked;
            BindingContext = this;
        }

        private void SwitchErrorWithWarningMessage(bool messageRectangleVisibility,
                                                   bool errorHeaderVisibility, bool errorShapeVisibility, bool errorRedLineVisibility, 
                                                   bool warningHeaderVisibility, bool warningShapeVisibility, bool warningYellowLineVisibility)
        {

            messageRectangle.IsVisible = messageRectangleVisibility;
            errorHeader.IsVisible = errorHeaderVisibility;
            errorShape.IsVisible = errorShapeVisibility;
            errorRedLine.IsVisible = errorRedLineVisibility;
            warningHeader.IsVisible = warningHeaderVisibility;
            warningShape.IsVisible = warningShapeVisibility;
            warningYellowLine.IsVisible = warningYellowLineVisibility;
        }

        private async void SimulateButtonClicked(object sender, EventArgs e)
        {
            try
            {
                if (iiCassoCode.Text != "" && iiCassoCode.Text.Length == 6)
                {
                    SwitchErrorWithWarningMessage(true,
                        false, false, false,
                        true, true, true);
                    label.Text = "Downloading could take a while";

                    HttpResponseMessage responseMessage = await _client.GetAsync(
                        "https://kukadrawingfunctionsapi.azurewebsites.net/api/drawing/" + iiCassoCode.Text.ToUpper() +
                        "/robotpath?appkey=ogLk8StwxrnSojn9owMrMWPYOoZBTBuoeUDa5PqESrRdAmxWj2WDlw==");
                    string responseContent = await responseMessage.Content.ReadAsStringAsync();
                    
                    if (responseContent != null && responseContent !=
                        "The path for the requested drawing is not available. Either the drawing was not registered yet, or it failed to register.")
                    {
                        Application.Current.MainPage = new MasterPage();
                    }
                    else
                    {
                        SwitchErrorWithWarningMessage(true,
                            true, true, true,
                            false, false, false);
                        label.Text = "Please enter a correct iiCasso code!";
                    }
                }
                else
                {
                    SwitchErrorWithWarningMessage(true,
                        true, true, true,
                        false, false, false);
                    label.Text = "Please enter the iiCasso code first!";
                }
            }
            catch (NullReferenceException)
            {
                SwitchErrorWithWarningMessage(true,
                    true, true, true,
                    false, false, false);
                label.Text = "Please enter the iiCasso code first!";
            }
            catch
            {
                SwitchErrorWithWarningMessage(true,
                    true, true, true,
                    false, false, false);
                label.Text = "Drawing could not bo accessed.";
            }
        }
    }
}
