﻿using System;
using Xamarin.Forms;

namespace XamarinApp.UnityView
{
    public partial class Unity3dView : View
    {
        [UnityInvokeEvent("OnUnitySceneIsReady")]
        public event EventHandler<CallableInfo.MethodInfo> OnUnitySceneIsReady;
        
        [UnityInvokeEvent("OnScreenshotTaken")]
        public event EventHandler<CallableInfo.MethodInfo<string, int>> OnUnityScreenshotTaken;

        [UnityInvokeEvent("OnAnimationLoadFinished")]
        public event EventHandler<CallableInfo.MethodInfo<double>> OnUnityAnimationLoadFinished;
    }
}