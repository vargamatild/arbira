﻿using System;

namespace XamarinApp.UnityView
{
    public class UnityCallableException : Exception
    {
        public UnityCallableException(string message) : base(message)
        {
        }
    }


    public class CallableInfo
    {
        public class MethodInfo
        {
            public MethodInfo(string[] args)
            {
                if (args.Length != 0)
                    throw new UnityCallableException("The number of arguments doesn't match the expected amount.");
            }
        }
        public class MethodInfo<InValue>
        {
            public InValue Param { get; set; }

            public MethodInfo(string[] args)
            {
                if (args.Length != 1)
                    throw new UnityCallableException("The number of arguments doesn't match the expected amount.");

                Param = (InValue)Convert.ChangeType(args[0], typeof(InValue));
            }

            public static implicit operator InValue(MethodInfo<InValue> methodInfo) => methodInfo.Param;
        }
        public class MethodInfo<InValue1, InValue2>
        {
            public InValue1 Param1 { get; set; }
            public InValue2 Param2 { get; set; }

            public MethodInfo(string[] args)
            {
                if (args.Length != 2)
                    throw new UnityCallableException("The number of arguments doesn't match the expected amount.");

                Param1 = (InValue1)Convert.ChangeType(args[0], typeof(InValue1));
                Param2 = (InValue2)Convert.ChangeType(args[1], typeof(InValue2));
            }

            public static implicit operator (InValue1 param1, InValue2 param2)(MethodInfo<InValue1, InValue2> methodInfo) => (methodInfo.Param1, methodInfo.Param2);
        }

        public interface IReturnValueStorage
        {
            string GetReturnValue();
        }
        public class FunctionInfo<ReturnValue> : MethodInfo, IReturnValueStorage
        {
            public ReturnValue ToReturn { get; set; }

            public FunctionInfo(string[] args) : base(args)
            { }

            public string GetReturnValue() => ToReturn.ToString();
        }
        public class FunctionInfo<ReturnValue, InValue> : MethodInfo<InValue>, IReturnValueStorage
        {
            public ReturnValue ToReturn { get; set; }

            public FunctionInfo(string[] args) : base(args)
            { }

            public string GetReturnValue() => ToReturn.ToString();
        }
    }
}