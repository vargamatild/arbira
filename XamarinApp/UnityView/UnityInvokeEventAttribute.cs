﻿using System;

namespace XamarinApp.UnityView
{
    [AttributeUsage(AttributeTargets.Event)]
    public class UnityInvokeEventAttribute : Attribute
    {
        public string unityCallableName;

        public UnityInvokeEventAttribute(string unityCallableName)
        {
            this.unityCallableName = unityCallableName;
        }
    }
}