﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace XamarinApp.UnityView
{
    public class UnityInvokationException : Exception
    {
        public UnityInvokationException(string message) : base(message)
        {
        }
    }

    public partial class Unity3dView : View
    {
        public Action DestroyView;
        public Action CreateView;

        public Action<string, string, string> UnitySendMessage;

        readonly Dictionary<string, FieldInfo> unityInvokeEventFields;

        protected ILogger log = DependencyService.Get<ILogger>();

        public Unity3dView()
        {
            //get all the events that are registered to handle unity calls
            var registeredEvents = typeof(Unity3dView)
                .GetEvents()
                .Where(f => f.IsDefined(typeof(UnityInvokeEventAttribute)));

            //get all their delegate fields and store them with their corresponding unity function name
            unityInvokeEventFields = registeredEvents.Select(e =>
                    new KeyValuePair<string, FieldInfo>(
                        (e.GetCustomAttribute(typeof(UnityInvokeEventAttribute)) as UnityInvokeEventAttribute)?
                        .unityCallableName,
                        typeof(Unity3dView).GetField(e.Name, BindingFlags.Instance | BindingFlags.NonPublic)))
                .ToDictionary(kv => kv.Key, kv => kv.Value);
        }

        public virtual void RaiseMessageFromUnity(string msg)
        {
            log.Debug($"Message from Unity: \"{msg}\"");

            var invokeInfo = JsonConvert.DeserializeObject<NativeMessageObject>(msg);

            //get the event corresponding to the callable(function or method) to be invoked
            if (!unityInvokeEventFields.TryGetValue(invokeInfo.functionName, out var eventField))
                throw new UnityInvokationException($"Unity invoked \"{invokeInfo.functionName}\", but no events are registered for this function.");

            if (!(eventField.GetValue(this) is MulticastDelegate eventDelegate))
                return;

            //get the details of the callable, then populate the invokation info with the arguments sent
            var callableType = eventField.FieldType.GetGenericArguments()[0];
            var callableInfo = callableType.GetConstructor(new[] { typeof(string[]) })
                .Invoke(new object[] { invokeInfo.functionArgs });

            //raise the event
            eventDelegate.DynamicInvoke(this, callableInfo);

            //if the callable is a function, send the return value back to unity
            if (GenericName(callableType.Name) != "FunctionInfo") return;

            var responseInfo = new NativeResponseObject
            {
                functionName = invokeInfo.functionName,
                returnValue = (callableInfo as CallableInfo.IReturnValueStorage)?.GetReturnValue()
            };

            UnitySendMessage?.Invoke("NativeCallListener", "NativeResponse", JsonConvert.SerializeObject(responseInfo));

            string GenericName(string name)
            {
                int genericArityIndex = name.IndexOf('`');
                if (genericArityIndex >= 0)
                    return name.Substring(0, genericArityIndex);

                return name;
            }
        }

        [Serializable]
        protected class NativeMessageObject
        {
            public string functionName;
            public string[] functionArgs;
        }

        [Serializable]
        protected class NativeResponseObject
        {
            public string functionName;
            public string returnValue;
        }
    }
}