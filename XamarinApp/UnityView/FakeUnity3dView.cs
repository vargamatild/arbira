﻿using System.Linq;
using Newtonsoft.Json;

namespace XamarinApp.UnityView
{
    public class FakeUnity3dView : Unity3dView
    {
        //ILogger log = DependencyService.Get<ILogger>();

        public FakeUnity3dView()
        {
            UnitySendMessage += (objectName, functionName, args) =>
                log.Debug($"To Unity: {objectName}.{functionName}({args})");

            //OnUnityDisplayMessage += (_, msg) => log.Debug($"From Unity: DisplayMessage: {msg}");
        }

        public void SimulateMessageFromUnity(string functionName, params object[] functionArgs)
        {
            var msgObj = new NativeMessageObject
            {
                functionName = functionName,
                functionArgs = functionArgs.Select(x => x.ToString()).ToArray()
            };

            var json = JsonConvert.SerializeObject(msgObj);

            log.Debug($"Sent message from Unity: {json}");
            RaiseMessageFromUnity(json);
        }
    }
}