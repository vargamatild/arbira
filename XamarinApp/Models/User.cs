﻿namespace XamarinApp.Models
{
    public class User
    {
        public string UserId { get; }
        public string FirstName { get; }
        public string LastName { get; }
        public string ImgPath { get; }

        public User(string userId, string firstName, string lastName, string imgPath)
        {
            UserId = userId;
            FirstName = firstName;
            LastName = lastName;
            ImgPath = imgPath;
        }


        public string GetName()
        {
            return FirstName + " " + LastName;
        }

        public static User FromDto(Api.User user)
        {
            return new User(user.Id, user.FirstName, user.LastName, user.ImgPath);
        }
    }
}