﻿namespace XamarinApp.Models
{
    public interface RobothPathDownloadCallback
    {
        void OnComplete(string robotPath);
    }
}