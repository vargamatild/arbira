﻿using Newtonsoft.Json;

namespace XamarinApp.Models
{
    public class RobotRotation
    {
        public int X;
        public int Y;
        public int Z;

        public RobotRotation()
        {
            X = Y = Z = 0;
        }

        public RobotRotation(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}