﻿namespace XamarinApp.Models
{
    public class Simulation
    {
        private string RobotPathJson { get; }

        public Simulation(string robotPathJson)
        {
            RobotPathJson = robotPathJson;
        }


        // TODO return proper simulation length
        public int getSimulationLength()
        {
            return 25000;
        }
    }
}