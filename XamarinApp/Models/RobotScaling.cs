﻿using Newtonsoft.Json;

namespace XamarinApp.Models
{
    public class RobotScaling
    {
        public int X;
        public int Y;
        public int Z;

        public RobotScaling()
        {
            X = Y = Z = 0;
        }

        public RobotScaling(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}