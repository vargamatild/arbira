﻿using System;
using System.Collections.Generic;

namespace XamarinApp.Models
{
    public class Feedback
    {
        public string Id { get; }
        public string UserId { get; }
        public DateTime CreationTime { get; }
        public string Text { get; }
        public List<Feedback> Feedbacks { get; }
        public string ParentFeedbackId { get; }
        public string SnapshotId { get; }

        public Feedback(string id, string userId, DateTime creationTime, string text, string parentFeedbackId, string snapshotId)
        {
            Id = id;
            UserId = userId;
            CreationTime = creationTime;
            Text = text;
            Feedbacks = new List<Feedback>();
            ParentFeedbackId = parentFeedbackId;
            SnapshotId = snapshotId;
        }
    }
}