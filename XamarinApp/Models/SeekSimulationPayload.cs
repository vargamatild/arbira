﻿using Newtonsoft.Json;

namespace XamarinApp.Models
{
    public class SeekSimulationPayload
    {
        public float NormalizedTime;

        public SeekSimulationPayload(float normalizedTime)
        {
            NormalizedTime = normalizedTime;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}