﻿using System;
using System.Collections.Generic;

namespace XamarinApp.Models
{
    public class Snapshot : IComparable<Snapshot>
    {
        public string SnapshotId { get; }
        public int Milliseconds { get; }
        public List<Feedback> Feedbacks { get; }
        public bool HasDrawings { get; set; }

        public Snapshot(string snapshotId, int milliseconds)
        {
            SnapshotId = snapshotId;
            Milliseconds = milliseconds;
            Feedbacks = new List<Feedback>();
            HasDrawings = false;
        }

        public static Snapshot FromDto(Api.Feedback feedback)
        {
            return new Snapshot(feedback.Id, feedback.PositionInMilliSeconds);
        }

        public int CompareTo(Snapshot other)
        {
            return Milliseconds.CompareTo(other.Milliseconds);
        }
    }
}