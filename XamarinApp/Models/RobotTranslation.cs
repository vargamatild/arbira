﻿using Newtonsoft.Json;

namespace XamarinApp.Models
{
    public class RobotTranslation
    {
        public int X;
        public int Y;
        public int Z;

        public RobotTranslation()
        {
            X = Y = Z = 0;
        }

        public RobotTranslation(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}