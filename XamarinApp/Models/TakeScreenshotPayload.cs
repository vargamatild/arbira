﻿using Newtonsoft.Json;

namespace XamarinApp.Models
{
    public class TakeScreenshotPayload
    {
        public string SnapshotId;
        public int Milliseconds;

        public TakeScreenshotPayload(string snapshotId, int milliseconds)
        {
            SnapshotId = snapshotId;
            Milliseconds = milliseconds;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}