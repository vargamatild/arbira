﻿using Newtonsoft.Json;

namespace XamarinApp.Models
{
    public class AppendRobotPathPiecePayload
    {
        public string RobotPathPiece;

        public AppendRobotPathPiecePayload(string robotPathPiece)
        {
            RobotPathPiece = robotPathPiece;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
