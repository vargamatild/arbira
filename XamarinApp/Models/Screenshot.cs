﻿using System.Collections.Generic;

namespace XamarinApp.Models
{
    public class Screenshot
    {
        public double value;
        public string imageUrl;
        public List<string> comments;
        public List<object> drawingCoordinates;

        public Screenshot(double value, string imageUrl, List<string> comments, List<object> drawingCoordinates)
        {
            this.value = value;
            this.imageUrl = imageUrl;
            this.comments = comments;
            this.drawingCoordinates = drawingCoordinates;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Screenshot screenshot)) return base.Equals(obj);
            return value == screenshot.value && imageUrl == screenshot.imageUrl;
        }
    }
}