﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinApp.Models;
using XamarinApp.ViewModels;

namespace XamarinApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FeedbackView : ContentView
    {
        ILogger log = DependencyService.Get<ILogger>();
        public static Screenshot ActiveIconOnTimeline { get; set; }
        private List<(float X, float Y)> _singleDrawingCoordinates;

        public static BindableProperty SimulationLengthProperty = BindableProperty.Create(nameof(SimulationLength),
            typeof(double), typeof(FeedbackView));
        public double SimulationLength
        {
            get => (double)GetValue(SimulationLengthProperty);
            set => SetValue(SimulationLengthProperty, value);
        }

        public double CanvasHeight { get; }

        public double CommentViewHeight { get; }

        static List<Screenshot> Screenshots => SimulationFeedbackViewModel.Screenshots.list;

        public FeedbackView()
        {
            InitializeComponent();

            BindingContext = new CanvasTestPageViewModel();

            RelativeLayout.SetHeightConstraint(OperationArea, Constraint.RelativeToParent((parent) => parent.Height - SimulationFeedbackView.NavbarHeight));
            RelativeLayout.SetYConstraint(CanvasCommands, Constraint.Constant(SimulationFeedbackView.NavbarHeight));

            CanvasHeight = OperationArea.Height - 100;
            CommentViewHeight = OperationArea.Height - 50;

            SimulationFeedbackViewModel.Screenshots.ScreenshotAdded += Screenshots_ScreenshotAdded;

            //BaseSlider.Maximum = SimulationLength;
        }

        private void Screenshots_ScreenshotAdded(Models.Screenshot screenshot)
        {
            //log.Debug("asd: " + snapshotIconValues.Count);
            //log.Debug("asd: " + String.Join(", ", snapshotIconValues.Select(x => x.imageUrl).ToArray()));
            var newSlider = new Slider
            {
                ThumbImageSource = "snapshotIconOnTimeline",
                BackgroundColor = Color.Transparent,
                MinimumTrackColor = Color.Transparent,
                MaximumTrackColor = Color.Transparent,
                Maximum = 50.0,
                IsEnabled = false,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Value = screenshot.value
            };

            RowWithTimeline.Children.Add(newSlider);
            SetActiveIconValue(Screenshots.Count - 1);
        }

        private void CanvasOnTouch(object sender, Canvas.TouchEventArgs e)
        {
            if (e.Action == Canvas.TouchEventArgs.TouchAction.Down)
            {
                DrawingCanvas.BeginDraw(e.X, e.Y);
                _singleDrawingCoordinates = new List<(float X, float Y)> { (e.X, e.Y) };

                return;
            }

            DrawingCanvas.Draw(e.X, e.Y);
            _singleDrawingCoordinates.Add((e.X, e.Y));

            if (e.Action == Canvas.TouchEventArgs.TouchAction.Up)
            {
                var index = Screenshots.FindIndex(x => x.Equals(ActiveIconOnTimeline));

                if (index < 0)
                    return;

                Screenshots[index].drawingCoordinates.Add(_singleDrawingCoordinates);
            }
        }


        private void NewEntryCompleted(object sender, EventArgs e)
        {
            Entry userInput = (Entry)sender;
            Label label = new Label
            {
                Text = " - " + userInput.Text + "\n(" + DateTime.Now + ")",
                TextColor = Color.White,
                FontSize = 20
            };
            commentViewStack.Children.Add(label);

            var index = Screenshots.FindIndex(x => x.Equals(ActiveIconOnTimeline));

            if (index < 0)
                return;

            Screenshots[index].comments.Add(label.Text);
            userInput.Text = "";
        }

        private void BaseSliderValueChanged(object sender, ValueChangedEventArgs e)
        {
            var value = ((Slider)sender).Value;

            if (Screenshots.Count == 0)
                return;

            var min = Screenshots[0].value;
            string imageUrl = Screenshots[0].imageUrl;
            ActiveIconOnTimeline = Screenshots[0];

            for (int i = 1; i < Screenshots.Count; i++)
            {
                var iconValue = Screenshots[i].value;
                if (Math.Abs(value - iconValue) < Math.Abs(value - min))
                {
                    min = Screenshots[i].value;
                    imageUrl = Screenshots[i].imageUrl;
                    ActiveIconOnTimeline = Screenshots[i];
                }
            }

            SetupCurrentMediaPositionData(TimeSpan.FromSeconds(min));

            SetupCurrentMediaComments();

            DrawingCanvas.Source = imageUrl;
        }


        private void SetupCurrentMediaComments()
        {
            commentViewStack.Children.Clear();

            var index = Screenshots.FindIndex(x => x.Equals(ActiveIconOnTimeline));

            if (index < 0)
                return;

            foreach (var comment in Screenshots[index].comments)
            {
                Label label = new Label
                {
                    Text = comment,
                    TextColor = Color.White,
                    FontSize = 20
                };
                commentViewStack.Children.Add(label);
            }
        }

        public static void ClearDrawingsOfTheCurrentMedia()
        {
            for (var i = 0; i < Screenshots.Count; i++)
            {
                if (Screenshots[i].value == ActiveIconOnTimeline.value &&
                    Screenshots[i].imageUrl == ActiveIconOnTimeline.imageUrl)
                {
                    Screenshots[i].drawingCoordinates.Clear();
                }
            }
        }

        private void SetupCurrentMediaPositionData(TimeSpan currentPlaybackPosition)
        {
            string formattingPattern = @"hh\:mm\:ss\.fff";

            // TODO SimulationLength
            if (TimeSpan.FromSeconds(BaseSlider.Maximum).Hours <= 0)
                formattingPattern = @"mm\:ss\.fff";

            var fullLengthString = TimeSpan.FromSeconds(BaseSlider.Maximum).ToString(formattingPattern);
            LabelPosition.Text = $"{currentPlaybackPosition.ToString(formattingPattern)}";
            LabelPositionEnd.Text = $"{fullLengthString}";
        }

        private void SetActiveIconValue(int index)
        {
            ActiveIconOnTimeline = Screenshots[index];
            SetupCurrentMediaPositionData(TimeSpan.FromSeconds(ActiveIconOnTimeline.value));
            SetupCurrentMediaComments();
            DrawingCanvas.Source = ActiveIconOnTimeline.imageUrl;
            BaseSlider.Value = ActiveIconOnTimeline.value;
        }

        private void PrevTapped(object sender, EventArgs e)
        {
            var index = Screenshots.FindIndex(x => x.Equals(ActiveIconOnTimeline));

            log.Debug("asd index prev: " + index);

            if (index > 0 && index < Screenshots.Count)
            {
                SetActiveIconValue(index - 1);
            }
        }

        private void NextTapped(object sender, EventArgs e)
        {
            var index = Screenshots.FindIndex(x => x.Equals(ActiveIconOnTimeline));

            log.Debug("asd index next: " + index);

            if (index > -1 && index < Screenshots.Count - 1)
            {
                SetActiveIconValue(index + 1);
            }
        }

    }
}