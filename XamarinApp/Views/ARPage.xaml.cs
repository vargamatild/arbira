﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinApp.ARPlayer;
using XamarinApp.ViewModels;

namespace XamarinApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ARPage : ContentPage
    {
        public ARPage()
        {
            InitializeComponent();

            BindingContext = new ARPageViewModel(SampleRobotPath.PATH1);
        }
    }
}