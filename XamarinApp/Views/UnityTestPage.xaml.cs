﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinApp.ViewModels;

namespace XamarinApp.Views
{
    public partial class UnityTestPage : ContentPage
    {
        readonly UnityTestPageViewModel viewModel;
        public UnityTestPage()
        {
            InitializeComponent();

            viewModel = new UnityTestPageViewModel();
            BindingContext = viewModel;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            unityView.CreateView?.Invoke();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            unityView.DestroyView?.Invoke();
        }

        private void Unity3dView_OnUnitySceneIsReady(object sender, UnityView.CallableInfo.MethodInfo e)
        {
            viewModel.UnityMessage = "Scene loaded.";
        }
    }
}