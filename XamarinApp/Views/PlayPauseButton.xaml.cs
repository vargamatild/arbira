﻿using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinApp.ViewModels;

namespace XamarinApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlayPauseButton : ContentView
    {
        public PlayPauseButtonViewModel ViewModel { get; set; }
        public static BindableProperty PlayingProperty = BindableProperty.Create(nameof(Playing),
            typeof(bool), typeof(PlayPauseButton), null, BindingMode.OneWayToSource);
        public bool Playing
        {
            get => (bool)GetValue(PlayingProperty);
            set => SetValue(PlayingProperty, value);
        }

        public PlayPauseButton()
        {
            InitializeComponent();

            ViewModel = new PlayPauseButtonViewModel();
            BindingContext = ViewModel;

            ViewModel.PlayingChanged += (_, playing) => Playing = playing;
        }

        public void SetVisibility(bool visibility)
        {
            PlayButton.IsVisible = visibility;
        }
    }
}