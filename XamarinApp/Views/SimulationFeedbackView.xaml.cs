﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinApp.ViewModels;

namespace XamarinApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SimulationFeedbackView : ContentView
    {
        public static double NavbarHeight { get; set; }
        SimulationFeedbackViewModel viewModel;
        public SimulationFeedbackView()
        {
            viewModel = new SimulationFeedbackViewModel();

            InitializeComponent();

            BindingContext = viewModel;

            NavbarHeight = Navbar.Height;

            RelativeLayout.SetHeightConstraint(feedbackView, Constraint.RelativeToParent((parent) => parent.Height - Navbar.Height));
            RelativeLayout.SetYConstraint(feedbackView, Constraint.RelativeToParent((parent) => parent.Y + Navbar.Height));
        }

        private void SimulationButton_Clicked(object sender, EventArgs e)
        {
            SwitchViews(true);
        }

        private void FeedbackButton_Clicked(object sender, EventArgs e)
        {
            if (SimulationFeedbackViewModel.Screenshots.list.Count == 0)
                return;

            SwitchViews(false);
        }

        void SwitchViews(bool showSimulation)
        {
            simulationView.IsVisible = showSimulation;
            feedbackView.IsVisible = !showSimulation;
            simulationButton.IsEnabled = !showSimulation;
            feedbackButton.IsEnabled = showSimulation;
        }
    }
}