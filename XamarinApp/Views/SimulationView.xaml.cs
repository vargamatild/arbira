﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinApp.ARPlayer;
using XamarinApp.ViewModels;

namespace XamarinApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SimulationView : ContentView
    {
        ILogger log = DependencyService.Get<ILogger>();
        
        public static SimulationView Instance { get; private set; }
        public SimulationView()
        {
            Instance = this;
            InitializeComponent();

            var viewModel = new ARPageViewModel(SampleRobotPath.PATH1);
            BindingContext = viewModel;
            
            arPlayer.CurrentTimeChanged += (_s, time) => AnimationSlider.Value = time;
            arPlayer.AnimationLengthChanged += (_s, length) => viewModel.SimulationLength = length;

            arPlayer.ScreenshotTakenSuccesfully += PutSnapshotIconOnTimeline;
        }

        private void SetupCurrentMediaPositionData(TimeSpan currentPlaybackPosition)
        {
            string formattingPattern = @"hh\:mm\:ss\.fff";
            if (TimeSpan.FromSeconds(AnimationSlider.Maximum).Hours <= 0)
                formattingPattern = @"mm\:ss\.fff";

            var fullLengthString = TimeSpan.FromSeconds(AnimationSlider.Maximum).ToString(formattingPattern);
            LabelPosition.Text = $"{currentPlaybackPosition.ToString(formattingPattern)}";
            LabelPositionEnd.Text = $"{fullLengthString}";
        }

        private void AnimationSlider_DragStarted(object sender, EventArgs e)
        {
            if (playButton.Playing)
                playButton.ViewModel.PlayPauseCommand?.Execute(null);
        }

        private void AnimationSliderDragCompleted(object sender, EventArgs e)
        {
            var slider = sender as Slider;
            if (slider != null) SeekTo(slider.Value);
        }

        private void SeekTo(double time)
        {
            if (playButton.Playing)
                playButton.ViewModel.PlayPauseCommand?.Execute(null);

            if (time > AnimationSlider.Maximum)
                time = AnimationSlider.Maximum;
            else if (time < 0)
                time = 0;

            arPlayer.SeekTo(time);
        }

        private void AnimationSlider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            var slider = sender as Slider;
            slider.Value = e.NewValue;
            SetupCurrentMediaPositionData(TimeSpan.FromSeconds(e.NewValue));
        }

        private void PutSnapshotIconOnTimeline(string imageUrl)
        {
            var currentPositionSeconds = arPlayer.AnimationCurrentTime;

            Slider newSlider = new Slider
            {
                ThumbImageSource = "snapshotIconOnTimeline",
                BackgroundColor = Color.Transparent,
                MinimumTrackColor = Color.Transparent,
                MaximumTrackColor = Color.Transparent,
                Maximum = arPlayer.AnimationLength,
                IsEnabled = false,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Value = currentPositionSeconds
            };
            RowWithTimeline.Children.Add(newSlider, 1, 0);
            SimulationFeedbackViewModel.Screenshots.Add(new Models.Screenshot(currentPositionSeconds, imageUrl, new List<string>(), new List<object>()));

            log.Debug("asd kurva log" + SimulationFeedbackViewModel.Screenshots.list.Count);
        }

        private void TimelineForward10(object sender, EventArgs e)
        {
            SeekTo(AnimationSlider.Value + 10);
        }

        private void TimelineForward5(object sender, EventArgs e)
        {
            SeekTo(AnimationSlider.Value + 5);
        }

        private void TimelineForward1(object sender, EventArgs e)
        {
            SeekTo(AnimationSlider.Value + 1);
        }

        private void TimelineBackward10(object sender, EventArgs e)
        {
            SeekTo(AnimationSlider.Value - 10);
        }

        private void TimelineBackward5(object sender, EventArgs e)
        {
            SeekTo(AnimationSlider.Value - 5);
        }

        private void TimelineBackward1(object sender, EventArgs e)
        {
            SeekTo(AnimationSlider.Value - 1);
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (DeviceDisplay.MainDisplayInfo.Orientation == DisplayOrientation.Portrait)
            {
                LeftEllipse.ScaleX = 1;
                RightEllipse.ScaleX = 1;
                LeftEllipse.ScaleY = 1;
                RightEllipse.ScaleY = 1;
                LeftTimelineButtons.RowDefinitions[1].Height = new GridLength(3, GridUnitType.Star);
                RightTimelineButtons.RowDefinitions[1].Height = new GridLength(3, GridUnitType.Star);
                MiddlePartColums.ColumnDefinitions[1].Width = new GridLength(4, GridUnitType.Star);
                return;
            }

            LeftEllipse.ScaleX = 2;
            RightEllipse.ScaleX = 2;
            LeftEllipse.ScaleY = 2;
            RightEllipse.ScaleY = 2;
            LeftTimelineButtons.RowDefinitions[1].Height = new GridLength(1, GridUnitType.Star);
            RightTimelineButtons.RowDefinitions[1].Height = new GridLength(1, GridUnitType.Star);
            MiddlePartColums.ColumnDefinitions[1].Width = new GridLength(6, GridUnitType.Star);
        }

        private void TakeSnapshot_Clicked(object sender, EventArgs e) => arPlayer.TakeScreenshot();

        
    }
}