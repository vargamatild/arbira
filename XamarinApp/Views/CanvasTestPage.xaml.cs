﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinApp.ViewModels;

namespace XamarinApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CanvasTestPage : ContentPage
    {
        (float x, float y) pos;

        public CanvasTestPage(string path)
        {
            InitializeComponent();

            BindingContext = new CanvasTestPageViewModel();

            ((NavigationPage)Application.Current.MainPage).BarBackgroundColor = Color.FromHex("#292336");
            ((NavigationPage)Application.Current.MainPage).BarTextColor = Color.White;

            BuildRowWithTimeline();
        }

        private void BuildRowWithTimeline()
        {
            var snapshotIconValues = SecondPage.SnapshotIconsValues;

            Slider newSlider = new Slider
            {
                ThumbColor = Color.Transparent,
                BackgroundColor = Color.Purple,
                MinimumTrackColor = Color.White,
                MaximumTrackColor = Color.White,
                Maximum = SecondPage.MediaItemDurationTotalSeconds,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Value = 0
            };
            RowWithTimeline.Children.Add(newSlider);


            foreach (var position in snapshotIconValues)
            {
                newSlider = new Slider
                {
                    ThumbImageSource = "snapshotIconOnTimeline",
                    BackgroundColor = Color.Transparent,
                    MinimumTrackColor = Color.Transparent,
                    MaximumTrackColor = Color.Transparent,
                    Maximum = SecondPage.MediaItemDurationTotalSeconds,
                    IsEnabled = false,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    Value = position
                };
                RowWithTimeline.Children.Add(newSlider);
            }

            var snapshotComments = SecondPage.SnapshotComments;
            foreach (var comment in snapshotComments)
            {
                Label label = new Label
                {
                    Text = comment,
                    TextColor = Color.White,
                    FontSize = 20
                };
                commentViewStack.Children.Add(label);
            }
        }

        private void Canvas_OnTouch(object sender, Canvas.TouchEventArgs e)
        {
            if (e.Action == Canvas.TouchEventArgs.TouchAction.Down)
            {
                canvas.BeginDraw(e.X, e.Y);
                pos = (e.X, e.Y);

                return;
            }

            canvas.Draw(e.X, e.Y);
        }

        private void NewEntryCompleted(object sender, EventArgs e)
        {
            Entry userInput = (Entry)sender;
            Label label = new Label
            {
                Text = " - " + userInput.Text,
                TextColor = Color.White,
                FontSize = 20
            };
            commentViewStack.Children.Add(label);
            SecondPage.SnapshotComments.Add(label.Text);
            userInput.Text = "";
        }

        private async void SimulationButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}