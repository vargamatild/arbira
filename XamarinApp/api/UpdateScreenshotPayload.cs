﻿namespace XamarinApp.Api
{
    public class UpdateScreenshotPayload
    {
        public string Img { get; }

        public UpdateScreenshotPayload(string img)
        {
            Img = img;
        }
    }
}