﻿namespace XamarinApp.Api
{
    public class Robot
    {
        public string Id { get; }
        public string UserId { get; }
        public string ModelName { get; }
        public string ModelFile { get; }
        public string Manufacturer { get; }
        public string Description { get; }

        public Robot(string id, string userId, string modelName, string modelFile, string manufacturer, string description)
        {
            Id = id;
            UserId = userId;
            ModelName = modelName;
            ModelFile = modelFile;
            Manufacturer = manufacturer;
            Description = description;
        }
    }
}