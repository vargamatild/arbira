﻿namespace XamarinApp.Api
{
    public class Comment
    {
        public string Id { get; }
        public string UserId { get; }
        public string FeedbackId { get; }
        public string CreationTime { get; }
        public string Text { get; }
        public string ParentCommentId { get; }

        public Comment(string id, string userId, string feedbackId, string creationTime, string text, string parentCommentId)
        {
            Id = id;
            UserId = userId;
            FeedbackId = feedbackId;
            CreationTime = creationTime;
            Text = text;
            ParentCommentId = parentCommentId;
        }
    }
}