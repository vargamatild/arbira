﻿using System;

namespace XamarinApp.Api
{
    public class Simulation
    {
        public string SimulationId { get; }
        public string Name { get; }
        public string RobotId { get; }
        public string UserId { get; }
        public DateTime CreationTime { get; }
        public int LengthInMilliSeconds { get; }
        public string Description { get; }
        public string ScreenCapture { get; }
        public string SimulationFile { get; }

        public Simulation(string simulationId, string name, string robotId, string userId, DateTime creationTime, int lengthInMilliSeconds, string description, string screenCapture, string simulationFile)
        {
            SimulationId = simulationId;
            Name = name;
            RobotId = robotId;
            UserId = userId;
            CreationTime = creationTime;
            LengthInMilliSeconds = lengthInMilliSeconds;
            Description = description;
            ScreenCapture = screenCapture;
            SimulationFile = simulationFile;
        }
    }
}