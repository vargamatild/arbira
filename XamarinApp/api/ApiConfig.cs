﻿namespace XamarinApp.Api
{
    public class ApiConfig
    {
        public string BaseUrl { get; }

        public ApiConfig(string baseUrl)
        {
            BaseUrl = baseUrl;
        }
    }
}