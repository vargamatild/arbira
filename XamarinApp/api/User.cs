﻿namespace XamarinApp.Api
{
    public class User
    {
        public string Id { get; }
        public string FirstName { get; }
        public  string LastName { get; }
        public string ImgPath { get; }
        public string Email { get; }

        public User(string id, string firstName, string lastName, string imgPath, string email)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            ImgPath = imgPath;
            Email = email;
        }
    }
}