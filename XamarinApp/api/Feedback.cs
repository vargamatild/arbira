﻿namespace XamarinApp.Api
{
    public class Feedback
    {
        public string Id { get; }

        public string Img { get; }
        public int PositionInMilliSeconds { get; }

        public Feedback(string id, string img, int positionInMilliSeconds)
        {
            Id = id;
            Img = img;
            PositionInMilliSeconds = positionInMilliSeconds;
        }
    }
}