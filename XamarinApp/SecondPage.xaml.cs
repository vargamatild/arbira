﻿using MediaManager;
using NativeMedia;
using Plugin.Toast;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinApp.ARPlayer;
using XamarinApp.ViewModels;
using XamarinApp.Views;
using PositionChangedEventArgs = MediaManager.Playback.PositionChangedEventArgs;

namespace XamarinApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SecondPage : ContentPage
    {
        ILogger log = DependencyService.Get<ILogger>();

        private readonly double _width = DeviceDisplay.MainDisplayInfo.Width;
        private readonly double _height = DeviceDisplay.MainDisplayInfo.Height;
        public static double MediaItemDurationTotalSeconds { get; set; }
        public string imageUrl = "picture.png";
        public static List<double> SnapshotIconsValues { get; set; }
        public static List<string> SnapshotComments { get; set; }

        public double DisplayWidth
        {
            get
            {
                if (_width < _height)
                    return _width - 140;
                return _height - 140;
            }
        }

        public double DisplayHeight
        {
            get
            {
                if (_width > _height)
                    return _width - 160;
                return _height - 160;
            }
        }


        public SecondPage()
        {
            InitializeComponent();
            SnapshotIconsValues = new List<double>();
            SnapshotComments = new List<string>();

            BindingContext = new ARPageViewModel(SampleRobotPath.PATH1);

            arPlayer.CurrentTimeChanged += (_s, time) => AnimationSlider.Value = time;

            //arPlayer.ScreenshotTakenSuccesfully += PutSnapshotIconOnTimeline;
        }

        private void SetupCurrentMediaPositionData(TimeSpan currentPlaybackPosition)
        {
            string formattingPattern = @"hh\:mm\:ss\.fff";
            if (TimeSpan.FromSeconds(arPlayer.AnimationLength).Hours <= 0)
                formattingPattern = @"mm\:ss\.fff";

            var fullLengthString = arPlayer.AnimationLength.ToString(formattingPattern);
            LabelPosition.Text = $"{currentPlaybackPosition.ToString(formattingPattern)}";
            LabelPositionEnd.Text = $"{fullLengthString}";

            AnimationSlider.Value = currentPlaybackPosition.Ticks;
        }

        private void AnimationSliderDragCompleted(object sender, EventArgs e)
        {
            //var slider = sender as Slider;
            //CrossMediaManager.Current.SeekTo(TimeSpan.FromSeconds(slider.Value));
        }

        private void PutSnapshotIconOnTimeline()
        {
            var currentPositionSeconds = arPlayer.AnimationCurrentTime;

            Slider newSlider = new Slider
            {
                ThumbImageSource = "snapshotIconOnTimeline",
                BackgroundColor = Color.Transparent,
                MinimumTrackColor = Color.Transparent,
                MaximumTrackColor = Color.Transparent,
                Maximum = arPlayer.AnimationLength,
                IsEnabled = false,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Value = currentPositionSeconds
            };
            RowWithTimeline.Children.Add(newSlider, 1, 0);
            SnapshotIconsValues.Add(currentPositionSeconds);
        }

        private void TimelineForward10(object sender, EventArgs e)
        {

        }

        private void TimelineForward5(object sender, EventArgs e)
        {

        }

        private void TimelineForward1(object sender, EventArgs e)
        {

        }

        private void TimelineBackward10(object sender, EventArgs e)
        {

        }

        private void TimelineBackward5(object sender, EventArgs e)
        {

        }

        private void TimelineBackward1(object sender, EventArgs e)
        {

        }

        protected override bool OnBackButtonPressed() => true;

        private async void FeedbackButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CanvasTestPage(imageUrl));
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (DeviceDisplay.MainDisplayInfo.Orientation == DisplayOrientation.Portrait)
            {
                LeftEllipse.ScaleX = 1;
                RightEllipse.ScaleX = 1;
                LeftEllipse.ScaleY = 1;
                RightEllipse.ScaleY = 1;
                LeftTimelineButtons.RowDefinitions[1].Height = new GridLength(3, GridUnitType.Star);
                RightTimelineButtons.RowDefinitions[1].Height = new GridLength(3, GridUnitType.Star);
                MiddlePartColums.ColumnDefinitions[1].Width = new GridLength(4, GridUnitType.Star);
                return;
            }

            LeftEllipse.ScaleX = 2;
            RightEllipse.ScaleX = 2;
            LeftEllipse.ScaleY = 2;
            RightEllipse.ScaleY = 2;
            LeftTimelineButtons.RowDefinitions[1].Height = new GridLength(1, GridUnitType.Star);
            RightTimelineButtons.RowDefinitions[1].Height = new GridLength(1, GridUnitType.Star);
            MiddlePartColums.ColumnDefinitions[1].Width = new GridLength(6, GridUnitType.Star);
        }

        private void arPlayer_OnUnityScreenshotTaken(object sender, UnityView.CallableInfo.MethodInfo<string> e)
        {

        }
    }
}