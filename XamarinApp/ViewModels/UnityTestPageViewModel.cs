﻿namespace XamarinApp.ViewModels
{
    public class UnityTestPageViewModel : BaseViewModel
    {
        string unityMessage;

        public string UnityMessage
        {
            get => unityMessage;
            set => SetProperty(ref unityMessage, value);
        }

        public UnityTestPageViewModel()
        {
            unityMessage = "Messages from Unity appear here.";
        }
    }
}