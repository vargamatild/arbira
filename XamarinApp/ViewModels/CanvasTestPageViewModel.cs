﻿using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace XamarinApp.ViewModels
{
    public class CanvasTestPageViewModel : BaseViewModel
    {
        ICommand clearCanvasCommand;
        public ICommand ClearCanvasCommand
        {
            get => clearCanvasCommand;
            set => SetProperty(ref clearCanvasCommand, value);
        }

        ICommand saveCanvasCommand;
        public ICommand SaveCanvasCommand
        {
            get => saveCanvasCommand;
            set => SetProperty(ref saveCanvasCommand, value);
        }

        private ICommand addCommentToCanvasCommand;

        public ICommand AddCommentToCanvasCommand
        {
            get => addCommentToCanvasCommand;
            set => SetProperty(ref addCommentToCanvasCommand, value);
        }

        public CanvasTestPageViewModel()
        {
        }
    }
}