﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace XamarinApp.ViewModels
{
    public class PlayPauseButtonViewModel : BaseViewModel
    {
        private ILogger log = DependencyService.Get<ILogger>();
        bool playing;
        public bool Playing
        {
            get => playing;
            set => SetProperty(ref playing, value);
        }

        ICommand playPauseCommand;
        public ICommand PlayPauseCommand
        {
            get => playPauseCommand;
            set => SetProperty(ref playPauseCommand, value);
        }

        public event EventHandler<bool> PlayingChanged;

        public PlayPauseButtonViewModel()
        {
            Playing = false;
            PlayPauseCommand = new Command(() =>
            {
                Playing = !Playing;
                PlayingChanged?.Invoke(this, Playing);
                log.Debug("asd playcommand " + Playing);
            });
        }
    }
}