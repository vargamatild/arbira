﻿using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;
using XamarinApp.Models;

namespace XamarinApp.ViewModels
{
    public class SimulationFeedbackViewModel : BaseViewModel
    {
        ILogger log = DependencyService.Get<ILogger>();

        bool simulationActive;
        public bool SimulationActive
        {
            get => simulationActive;
            set => SetProperty(ref simulationActive, value);
        }

        ICommand showSimulation;
        public ICommand ShowSimulation
        {
            get => showSimulation;
            set => SetProperty(ref showSimulation, value);
        }

        ICommand showFeedback;
        public ICommand ShowFeedback
        {
            get => showFeedback;
            set => SetProperty(ref showFeedback, value);
        }

        public static ScreenshotList Screenshots { get; set; }

        public SimulationFeedbackViewModel()
        {
            SimulationActive = true;
            Screenshots = new ScreenshotList();
        }
    }
}