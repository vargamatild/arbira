﻿using System.Windows.Input;

namespace XamarinApp.ViewModels
{
    public class ARPageViewModel : BaseViewModel
    {
        string robotPath;

        public string RobotPath
        {
            get => robotPath;
            set => SetProperty(ref robotPath, value);
        }


        ICommand takeScreenshotCommand;
        public ICommand TakeScreenshotCommand
        {
            get => takeScreenshotCommand;
            set => SetProperty(ref takeScreenshotCommand, value);
        }

        double simulationLength;
        public double SimulationLength
        {
            get => simulationLength;
            set => SetProperty(ref simulationLength, value);
        }

        public ARPageViewModel(string robotPath)
        {
            this.robotPath = robotPath;
        }
    }
}