﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace XamarinApp
{
    public class Canvas : View
    {
        public Action Clear;

        public static BindableProperty ClearCommandProperty = BindableProperty.Create(nameof(ClearCommand),
            typeof(ICommand), typeof(Canvas), null, BindingMode.OneWayToSource);
        public ICommand ClearCommand
        {
            get => (ICommand)GetValue(ClearCommandProperty);
            set => SetValue(ClearCommandProperty, value);
        }

        public Action Save;

        public static BindableProperty SaveCommandProperty = BindableProperty.Create(nameof(SaveCommand),
            typeof(ICommand), typeof(Canvas), null, BindingMode.OneWayToSource);
        public ICommand SaveCommand
        {
            get => (ICommand)GetValue(SaveCommandProperty);
            set => SetValue(SaveCommandProperty, value);
        }

        public Action<ImageSource> SourceChanged;

        public static BindableProperty SourceProperty = BindableProperty.Create(nameof(ImageSource),
            typeof(ImageSource), typeof(Canvas),
            propertyChanged: (obj, oldValue, newValue) => ((Canvas)obj).SourceChanged?.Invoke(newValue as ImageSource));
        public ImageSource Source
        {
            get => (ImageSource)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }

        public class TouchEventArgs
        {
            public enum TouchAction
            {
                Down, Up, Move
            }
            public TouchAction Action { get; }
            public float X { get; }
            public float Y { get; }

            public TouchEventArgs(TouchAction action, float x, float y)
            {
                Action = action;
                X = x;
                Y = y;
            }
            public TouchEventArgs(TouchAction action, (float x, float y) pos)
            {
                Action = action;
                X = pos.x;
                Y = pos.y;
            }
        }

        public event EventHandler<TouchEventArgs> OnTouch;
        public void RaiseTouchEvent(TouchEventArgs args) => OnTouch?.Invoke(this, args);

        public Action<float, float> OnBeginDraw;
        public Action<float, float> OnDraw;
        public Action<float, float, string> OnDrawText;

        public void BeginDraw(float x, float y) => OnBeginDraw?.Invoke(x, y);
        public void Draw(float x, float y) => OnDraw?.Invoke(x, y);

        public Canvas()
        {
            ClearCommand = new Command(() => Clear?.Invoke());
            SaveCommand = new Command(() => Save?.Invoke());
        }
    }
}