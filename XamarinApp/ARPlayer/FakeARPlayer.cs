﻿using System;
using Xamarin.Forms;

namespace XamarinApp.ARPlayer
{
    public class FakeARPlayer : ARPlayer
    {
        public new static BindableProperty RobotPathProperty = BindableProperty.Create(nameof(RobotPath),
            typeof(string), typeof(ARPlayer));

        public new event Action ScreenshotTakenSuccesfully;

        public FakeARPlayer()
        {
            isSceneLoaded = true;
            isRobotPathLoaded = true;

            UnitySendMessage += (obj, func, args) => log.Debug($"Sending message: {obj}.{func}");

            AnimationLength = 50;
            currentTime = new ValueAnimator(0, AnimationLength, 0.1);

            currentTime.ValueChanged += (_sender1, time) => AnimationCurrentTime = time;

            TakeScreenshotCommand = new Command((milliseconds) => ScreenshotTakenSuccesfully?.Invoke());
        }
    }
}