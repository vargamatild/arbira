﻿using PCLStorage;
using Plugin.Toast;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XamarinApp.UnityView;

namespace XamarinApp.ARPlayer
{
    public partial class ARPlayer : Unity3dView
    {
        ILogger log = DependencyService.Get<ILogger>();

        protected bool isSceneLoaded;
        protected bool isRobotPathLoaded;
        protected bool isAnimationLoaded;

        public static BindableProperty RobotPathProperty = BindableProperty.Create(nameof(RobotPath),
            typeof(string), typeof(ARPlayer), propertyChanged: (obj, oldValue, newValue) =>
            {
                var player = (ARPlayer)obj;
                if (player.isSceneLoaded && newValue != null && !player.isRobotPathLoaded) player.SendRobotPathToUnity();
            });
        public string RobotPath
        {
            get => (string)GetValue(RobotPathProperty);
            set => SetValue(RobotPathProperty, value);
        }

        public static BindableProperty SimulationPlayingProperty = BindableProperty.Create(nameof(SimulationPlaying),
            typeof(bool), typeof(ARPlayer), propertyChanged: (obj, oldValue, newValue) =>
            {
                var player = (ARPlayer)obj;
                if (!player.isAnimationLoaded) return;

                if ((bool)newValue)
                {
                    player.PlaySimulation();
                    player.currentTime.Start();
                }
                else
                {
                    player.PauseSimulation();
                    player.currentTime.Pause();
                }
            });
        public bool SimulationPlaying
        {
            get => (bool)GetValue(SimulationPlayingProperty);
            set => SetValue(SimulationPlayingProperty, value);
        }

        public static BindableProperty TakeScreenshotCommandProperty = BindableProperty.Create(nameof(TakeScreenshotCommand),
            typeof(ICommand), typeof(ARPlayer), null, BindingMode.OneWayToSource);
        public ICommand TakeScreenshotCommand
        {
            get => (ICommand)GetValue(TakeScreenshotCommandProperty);
            set => SetValue(TakeScreenshotCommandProperty, value);
        }

        public static BindableProperty AnimationLengthProperty = BindableProperty.Create(nameof(AnimationLength),
            typeof(double), typeof(ARPlayer), 1.0, BindingMode.OneWayToSource);
        public double AnimationLength
        {
            get => (double)GetValue(AnimationLengthProperty);
            protected set => SetValue(AnimationLengthProperty, value);
        }

        //DONT BIND THIS TO ANYTHING
        public static BindableProperty AnimationCurrentTimeProperty = BindableProperty.Create(nameof(AnimationCurrentTime),
            typeof(double), typeof(ARPlayer), 0.0, BindingMode.OneWayToSource);
        public double AnimationCurrentTime
        {
            get => (double)GetValue(AnimationCurrentTimeProperty);
            protected set => SetValue(AnimationCurrentTimeProperty, value);
        }

        protected ValueAnimator currentTime;
        public event EventHandler<double> CurrentTimeChanged;
        public event EventHandler<double> AnimationLengthChanged;
        public event Action<string> ScreenshotTakenSuccesfully;

        public ARPlayer()
        {
            UnitySendMessage += (obj, func, args) => log.Debug($"Sending message: {obj}.{func}");

            OnUnitySceneIsReady += (_s, _m) =>
            {
                isSceneLoaded = true;
                if (RobotPath != null) SendRobotPathToUnity();
            };

            OnUnityAnimationLoadFinished += (_sender, length) =>
            {
                AnimationLength = length;
                AnimationLengthChanged?.Invoke(this, AnimationLength);

                currentTime = new ValueAnimator(0, length, 0.1);

                currentTime.ValueChanged += (_sender1, time) => AnimationCurrentTime = time;
                currentTime.ValueChanged += CurrentTimeChanged;

                isAnimationLoaded = true;
            };

            TakeScreenshotCommand = new Command(TakeScreenshot);

            OnUnityScreenshotTaken += OnUnityScreenshotTakenHandler;
        }

        public void SeekTo(double time)
        {
            AnimationCurrentTime = time;
            CurrentTimeChanged?.Invoke(this, time);
            currentTime.SeekTo(time);
            SeekSimulation(new Models.SeekSimulationPayload((float)(time / AnimationLength)));
        }

        public void TakeScreenshot()
        {
            TakeScreenshot(new Models.TakeScreenshotPayload(Guid.NewGuid().ToString(), (int) (AnimationCurrentTime * 1000)));
        }

        private async void OnUnityScreenshotTakenHandler(object sender, CallableInfo.MethodInfo<string, int> e)
        {
            // TODO file path(xamarinapp + android + ios)

            var folder = await FileSystem.Current.GetFolderFromPathAsync("/storage/emulated/0/Android/data/com.companyname.xamarinapp/files");
            var filename = e.Param1 + ".png";

            bool screenshotTakenSuccessFully = false;

            int retryCount = 10;

            do
            {
                try
                {
                    await folder.GetFileAsync(filename);

                    screenshotTakenSuccessFully = true;
                    break;
                }
                catch (FileNotFoundException exception)
                {
                    await Task.Delay(1000);
                }
            } while (retryCount-- > 0);

            IFolder movedFolder = null;
            var movedFilename = "";
            if (screenshotTakenSuccessFully)
            {
                try
                {
                    // move the screenshot file to media directory
                    var picturesFolder = await FileSystem.Current.GetFolderFromPathAsync("/storage/emulated/0/Pictures/");
                    movedFolder = await picturesFolder.CreateFolderAsync("com.exliteron.arbira", CreationCollisionOption.OpenIfExists);
                    movedFilename = e.Param1 + "_orig.png";
                    File.Copy(Path.Combine(folder.Path, filename), Path.Combine(movedFolder.Path, movedFilename));
                }
                catch
                {
                    screenshotTakenSuccessFully = false;
                }
            }

            if (screenshotTakenSuccessFully)
            {
                ScreenshotTakenSuccesfully?.Invoke(Path.Combine(movedFolder.Path, movedFilename));
                CrossToastPopUp.Current.ShowToastMessage("Snapshot created.");
            }
            else
            {
                CrossToastPopUp.Current.ShowToastMessage("Failed to create snapshot.");
            }
        }

        void SendRobotPathToUnity()
        {
            //TODO send in pieces
            AppendRobotPathPiece(new Models.AppendRobotPathPiecePayload(RobotPath));
            RobotPathTransferFinished();

            isRobotPathLoaded = true;
        }

        public class RobotPathObj
        {
            public PathElement[] DrawingRobotPathList;
        }

        public class PathElement
        {
            public float time;
            public bool isDrawing;
            public float[] joint;
            public float[] drawingPosition;
        }
    }
}