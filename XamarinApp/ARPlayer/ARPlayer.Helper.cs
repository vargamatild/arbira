﻿using System;
using System.Collections.Generic;
using System.Text;
using XamarinApp.Models;

namespace XamarinApp.ARPlayer
{
    public partial class ARPlayer
    {
        public void SetRobotTranslation(RobotTranslation robotTranslation) => SendMessageToUnityPlayer(GameObject.RobotController, "SetRobotTranslation", robotTranslation.ToJson());

        public void SetRobotRotation(RobotRotation robotRotation) => SendMessageToUnityPlayer(GameObject.RobotController, "SetRobotRotation", robotRotation.ToJson());

        public void SetRobotScaling(RobotScaling robotScaling) => SendMessageToUnityPlayer(GameObject.RobotController, "SetRobotScaling", robotScaling.ToJson());

        public void TakeScreenshot(TakeScreenshotPayload payload) => SendMessageToUnityPlayer(GameObject.RobotController, "TakeScreenshot", payload.ToJson());

        public void AppendRobotPathPiece(AppendRobotPathPiecePayload payload) => SendMessageToUnityPlayer(GameObject.RobotController, "AppendRobotPathPiece", payload.ToJson());

        public void RobotPathTransferFinished() => SendMessageToUnityPlayer(GameObject.RobotController, "RobotPathTransferFinished");

        public void PlaySimulation() => SendMessageToUnityPlayer(GameObject.RobotController, "PlaySimulation");

        public void PauseSimulation() => SendMessageToUnityPlayer(GameObject.RobotController, "PauseSimulation");

        public void SeekSimulation(SeekSimulationPayload payload) => SendMessageToUnityPlayer(GameObject.RobotController, "SeekSimulation", payload.ToJson());

        void SendMessageToUnityPlayer(GameObject gameObject, string method, string message) => UnitySendMessage(gameObject.Name, method, message);

        void SendMessageToUnityPlayer(GameObject gameObject, string method) => SendMessageToUnityPlayer(gameObject, method, "");
    }
}
