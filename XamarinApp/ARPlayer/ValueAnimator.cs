﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace XamarinApp.ARPlayer
{
    public class ValueAnimator
    {
        public double Value { get; private set; }
        double minValue, maxValue;
        double rate;

        DateTime lastTick;
        double lastDiff = 0;
        bool active;

        ILogger log = DependencyService.Get<ILogger>();

        class TimerToken
        {
            bool active = true;
            ValueAnimator valueAnimator;

            public TimerToken(ValueAnimator valueAnimator)
            {
                this.valueAnimator = valueAnimator;
            }

            public bool Active
            {
                get => active && valueAnimator.Value < valueAnimator.maxValue;
                set => active = value;
            }
        }

        List<TimerToken> timerTokens;

        public ValueAnimator(double minValue, double maxValue, double rate)
        {
            this.minValue = minValue;
            this.maxValue = maxValue;
            this.rate = rate;

            Value = minValue;

            timerTokens = new List<TimerToken>();
        }

        public void Start()
        {
            log.Debug("asd active start elejen:" + active);
            if (active) return;


            Value += lastDiff;
            var token = new TimerToken(this);

            lastTick = DateTime.Now;

            Device.StartTimer(TimeSpan.FromSeconds(rate), () =>
            {
                //log.Debug($"helo {minValue} {maxValue} {Value}");
                if (!token.Active)
                {
                    lock (timerTokens)
                    {
                        timerTokens.Remove(token);
                    }

                    return false;
                }

                lastTick = DateTime.Now;

                Value = Math.Min(Value + rate, maxValue);
                ValueChanged?.Invoke(this, Value);

                return true;
            });

            active = true;

            lock (timerTokens)
            {
                timerTokens.Add(token);
            }

            log.Debug("asd active start: " + active);
        }

        public void Pause() => _Stop((DateTime.Now - lastTick).TotalSeconds);

        public void Stop()
        {
            _Stop(0);
            Value = minValue;
        }

        public void SeekTo(double time)
        {
            Pause();
            Value = time;
            lastDiff = 0;
            log.Debug("asd active seeknel:" + active);
        }

        void _Stop(double diff)
        {
            if (!active) return;

            lock (timerTokens)
            {
                foreach (var token in timerTokens)
                    token.Active = false;
            }

            lastDiff = diff;
            active = false;
        }

        public event EventHandler<double> ValueChanged;
    }
}