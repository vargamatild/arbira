﻿namespace XamarinApp.ARPlayer
{
    public class GameObject
    {
        public string Name { get; }

        private GameObject(string name)
        {
            Name = name;
        }

        public static GameObject RobotController = new GameObject("RobotController");
    }
}