﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinApp.Views;

namespace XamarinApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ButtonsView : ContentView
    {
        public ButtonsView()
        {
            InitializeComponent();
        }

        public ButtonsView(string imageButton10Source, string imageButton10Event, string imageButton5Source, string imageButton5Event, string imageButton1Source, string imageButton1Event)
        {
            InitializeComponent();

            GridImageButton10.Source = imageButton10Source;
            ImageButton10.Source = imageButton10Source;
            GridImageButton5.Source = imageButton5Source;
            ImageButton5.Source = imageButton5Source;
            GridImageButton1.Source = imageButton1Source;
            ImageButton1.Source = imageButton1Source;

            CreateButtonsEvents(imageButton10Event, imageButton5Event, imageButton1Event);
        }

        private void CreateButtonsEvents(string imageButton10Event, string imageButton5Event, string imageButton1Event)
        {
            ImageButton10.Clicked += GetEventHandler(imageButton10Event);
            GridImageButton10.Clicked += GetEventHandler(imageButton10Event);
            ImageButton5.Clicked += GetEventHandler(imageButton5Event);
            GridImageButton5.Clicked += GetEventHandler(imageButton5Event);
            ImageButton1.Clicked += GetEventHandler(imageButton1Event);
            GridImageButton1.Clicked += GetEventHandler(imageButton1Event);
        }

        private EventHandler GetEventHandler(string eventName)
        {
            return (sender, e) => typeof(SimulationView).GetMethod(eventName,
                    System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic)
                ?.Invoke(SimulationView.Instance, new[] { sender, e });
        }

        public void SetupButtonsVisibility(bool gridButtons, bool imageButton10, bool imageButton5, bool imageButton1)
        {
            GridButtons.IsVisible = gridButtons;
            ImageButton10.IsVisible = imageButton10;
            ImageButton5.IsVisible = imageButton5;
            ImageButton1.IsVisible = imageButton1;
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (DeviceDisplay.MainDisplayInfo.Orientation == DisplayOrientation.Portrait)
            {
                SetupButtonsVisibility(true, false, false, false);
                return;
            }

            SetupButtonsVisibility(false, true, true, true);
        }
    }
}