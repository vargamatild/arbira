﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

namespace XamarinApp.Droid.Utility
{
    public static class ImageSourceExtensions
    {
        public static IImageSourceHandler GetHandler(this ImageSource source)
        {
            if (source is UriImageSource)
                return new ImageLoaderSourceHandler();
            if (source is FileImageSource)
                return new FileImageSourceHandler();
            if (source is StreamImageSource)
                return new StreamImagesourceHandler();

            return null;
        }
    }
}