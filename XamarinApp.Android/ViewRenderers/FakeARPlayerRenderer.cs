﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using XamarinApp.ARPlayer;
using XamarinApp.Droid.ViewRenderers;

[assembly: ExportRenderer(typeof(FakeARPlayer), typeof(FakeARPlayerRenderer))]
namespace XamarinApp.Droid.ViewRenderers
{
    //derived classes apparently inherit renderers too, this class is here to "downgrade" the renderer to the default manually
    public class FakeARPlayerRenderer : ViewRenderer<FakeARPlayer, global::Android.Views.View>
    { }
}