﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using XamarinApp.ARPlayer;
using XamarinApp.Droid.ViewRenderers;
using XamarinApp.UnityView;

[assembly: ExportRenderer(typeof(FakeUnity3dView), typeof(FakeUnity3dViewRenderer))]
namespace XamarinApp.Droid.ViewRenderers
{
    //derived classes apparently inherit renderers too, this class is here to "downgrade" the renderer to the default manually
    public class FakeUnity3dViewRenderer : ViewRenderer<FakeUnity3dView, global::Android.Views.View>
    { }
}