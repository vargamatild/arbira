﻿using System;
using Android.Content;
using Android.Content.Res;
using Android.OS;
using Android.Runtime;
using Android.Views;
using AndroidX.Fragment.App;
using Com.Unity3d.Player;
using Process = Android.OS.Process;

namespace XamarinApp.Droid.ViewRenderers
{
    public class UnityPlayerFragment : Fragment, IUnityPlayerLifecycleEvents
    {
        public const string FragmentName = "UnityPlayerFragment";

        protected UnityPlayer mUnityPlayer;

        static WeakReference<IUnityMessageListener> weakUnityMessageListener;

        public class UnityMessageReceiver : BroadcastReceiver
        {
            public override void OnReceive(Context context, Intent intent)
            {
                var data = intent.GetStringExtra("data");
                if (weakUnityMessageListener.TryGetTarget(out var unityMessageListener))
                {
                    unityMessageListener.MessageFromUnity(data);
                }
            }
        }
        UnityMessageReceiver receiver;
        
        public static void SetUnityMessageListener(IUnityMessageListener unityMessageListener)
        {
            weakUnityMessageListener = new WeakReference<IUnityMessageListener>(unityMessageListener);
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
            receiver = new UnityMessageReceiver();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            mUnityPlayer = new UnityPlayer(Activity, this);

            mUnityPlayer.RequestFocus();
            return mUnityPlayer;
        }


        public void ProcessNewIntent(Intent intent)
        {
            mUnityPlayer.NewIntent(intent);
        }

        public void ProcessWindowFocusChanged(bool hasFocus)
        {
            mUnityPlayer.WindowFocusChanged(hasFocus);
        }

        public void ProcessTrimMemory([GeneratedEnum] TrimMemory level)
        {
            if (level == TrimMemory.RunningCritical)
            {
                //mUnityPlayer.LowMemory();
            }
        }

        public void UnitySendMessage(string gameObject, string function, string param)
        {
            UnityPlayer.UnitySendMessage(gameObject, function, param);
        }

        public override void OnPause()
        {
            base.OnPause();
            Activity.UnregisterReceiver(receiver);
            //mUnityPlayer.Pause();
        }

        public override void OnResume()
        {
            base.OnResume();
            Activity.RegisterReceiver(receiver, new IntentFilter("com.example.broadcast.UnityMessage"));
            mUnityPlayer.Resume();
        }

        public override void OnLowMemory()
        {
            base.OnLowMemory();
            //mUnityPlayer.LowMemory();
        }

        public override void OnConfigurationChanged(Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);
            //mUnityPlayer.ConfigurationChanged(newConfig);
        }


        #region IUnityPlayerLifecycleEvents

        public void OnUnityPlayerQuitted()
        {
            //Process.KillProcess(Process.MyPid());
        }

        public void OnUnityPlayerUnloaded()
        {
            //Activity.MoveTaskToBack(true);
        }

        #endregion IUnityPlayerLifecycleEvents
    }
}