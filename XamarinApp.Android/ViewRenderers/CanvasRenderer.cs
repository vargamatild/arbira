﻿using System;
using System.Collections.Generic;
using System.IO;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Views;
using Android.Widget;
using AndroidX.AppCompat.App;
using Plugin.Toast;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using XamarinApp.Droid.Utility;
using XamarinApp.Droid.ViewRenderers;
using XamarinApp.Views;
using Color = Android.Graphics.Color;
using Path = System.IO.Path;
using View = Android.Views.View;

[assembly: ExportRenderer(typeof(XamarinApp.Canvas), typeof(CanvasRenderer))]
namespace XamarinApp.Droid.ViewRenderers
{
    public class CanvasRenderer : ViewRenderer<Canvas, global::Android.Views.View>, View.IOnTouchListener
    {
        Context _context;

        ImageView imageView;

        Paint paint;
        Android.Graphics.Canvas tempCanvas;
        Bitmap tempBitmap, originalBitmap;

        (float x, float y) pos1;
        (float x, float y) pos2;
        (float x, float y) scale;

        View view;
        AppCompatActivity activity;

        public CanvasRenderer(Context context) : base(context)
        {
            _context = context;
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Canvas> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                e.OldElement.Clear -= Clear;
                e.OldElement.Save -= Save;
                e.OldElement.SourceChanged -= LoadImageFromSource;
                e.OldElement.OnBeginDraw -= BeginDraw;
                e.OldElement.OnDraw -= Draw;
            }

            if (e.NewElement != null)
            {
                activity = _context as AppCompatActivity;
                view = activity.LayoutInflater.Inflate(Resource.Layout.canvas_layout, this, false);

                e.NewElement.Clear += Clear;
                e.NewElement.Save += Save;
                e.NewElement.SourceChanged += LoadImageFromSource;
                e.NewElement.OnBeginDraw += BeginDraw;
                e.NewElement.OnDraw += Draw;

                SetNativeControl(view);

                Initialize();
                LoadImageFromSource(e.NewElement.Source);
            }
        }

        void Initialize()
        {
            imageView = view.FindViewById(Resource.Id.imageView1) as ImageView;

            paint = new Paint
            {
                Color = new Color(255, 0, 0), 
                StrokeWidth = 4,
                StrokeJoin = Paint.Join.Round,
                StrokeCap = Paint.Cap.Round
            };
            paint.SetStyle(Paint.Style.Stroke);

            imageView.SetOnTouchListener(this);

            tempCanvas = new Android.Graphics.Canvas();
        }

        void LoadImage(Bitmap bitmap)
        {
            if (bitmap == null) return;

            originalBitmap = bitmap.Copy(Bitmap.Config.Argb8888, true);
            imageView.SetImageBitmap(bitmap);

            tempBitmap = originalBitmap.Copy(Bitmap.Config.Argb8888, true);
            tempCanvas.SetBitmap(tempBitmap);
        }

        async void LoadImageFromSource(ImageSource source)
        {
            if (source != null) LoadImage(await source.GetHandler().LoadImageAsync(source, _context));

            if (FeedbackView.ActiveIconOnTimeline == null)
                return;

            var snapshotIconValues = FeedbackView.ActiveIconOnTimeline;

            foreach (var drawing in snapshotIconValues.drawingCoordinates)
            {
                List<(float X, float Y)> coordinates = (List<(float X, float Y)>)drawing;
                BeginDraw(coordinates[0].X, coordinates[0].Y);
                for (var i = 1; i < coordinates.Count; i++)
                {
                    Draw(coordinates[i].X, coordinates[i].Y);
                }
            }
        }

        void Clear()
        {
            FeedbackView.ClearDrawingsOfTheCurrentMedia();
            tempBitmap = originalBitmap.Copy(Bitmap.Config.Argb8888, true);
            tempCanvas.SetBitmap(tempBitmap);
            imageView.SetImageBitmap(tempBitmap);
        }

        /// <summary>
        /// Location of the saved photo: /files/internal storage/DCIM/Arbira/ArbiraDATE.png
        /// </summary>
        void Save()
        {
            var path = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDcim)?.AbsolutePath;
            var folderPath = Path.Combine(path, "Arbira");
            
            if (!File.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            var filename = "Arbira" + DateTime.Now.ToString("MMddyyyyHHmmss") + ".png";
            var filePath = Path.Combine(folderPath, filename);
            var stream = new FileStream(filePath, FileMode.Create);

            tempBitmap.Compress(Bitmap.CompressFormat.Png, 100, stream);
            stream.Close();

            CrossToastPopUp.Current.ShowToastMessage("Saved to " + filePath);
            Console.WriteLine(filePath);
        }

        public bool OnTouch(View? v, MotionEvent? e)
        {
            if (scale.x == 0 || scale.y == 0)
                scale = ((float)originalBitmap.Width / imageView.Width, (float)originalBitmap.Height / imageView.Height);

            if (e != null)
            {
                float x = e.GetX();
                float y = e.GetY();

                switch (e?.Action)
                {
                    case MotionEventActions.Down:
                        RaiseTouchEvent(Canvas.TouchEventArgs.TouchAction.Down, x, y);
                        break;
                    case MotionEventActions.Move:
                        RaiseTouchEvent(Canvas.TouchEventArgs.TouchAction.Move, x, y);
                        break;
                    case MotionEventActions.Up:
                        RaiseTouchEvent(Canvas.TouchEventArgs.TouchAction.Up, x, y);
                        break;
                }
            }

            return true;
        }

        void RaiseTouchEvent(Canvas.TouchEventArgs.TouchAction touchAction, float x, float y)
        {
            Element.RaiseTouchEvent(new Canvas.TouchEventArgs(touchAction, (x * scale.x, y * scale.y)));
        }

        void BeginDraw(float x, float y) => pos1 = (x, y);

        void Draw(float x, float y)
        {
            pos2 = (x, y);

            tempCanvas.DrawLine(pos1.x, pos1.y, pos2.x, pos2.y, paint);

            imageView.SetImageBitmap(tempBitmap);
            imageView.Invalidate();

            pos1 = pos2;
        }
    }
}