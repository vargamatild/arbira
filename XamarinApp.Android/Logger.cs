﻿using Android.Util;
using Xamarin.Forms;
using XamarinApp.Droid;

[assembly: Dependency(typeof(Logger))]
namespace XamarinApp.Droid
{
    public class Logger : ILogger
    {
        public void Info(string message) => Log.Info("ApplicationLog", message);
        public void Debug(string message) => Log.Debug("ApplicationLog", message);
        public void Warn(string message) => Log.Warn("ApplicationLog", message);
        public void Error(string message) => Log.Error("ApplicationLog", message);
    }
}